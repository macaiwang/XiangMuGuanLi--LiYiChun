from flask import Flask, render_template

app = Flask(__name__)


@app.route("/index")
@app.route("/")
def index():
    return "第一个页面"



@app.route("/profile")
def profile():
    return "个人中心页面"


@app.route("/profile2")
def profile2():
    with open("profile.html") as f:
        content = f.read()
        return content


@app.route("/profile3/10086")
def profile3():
    with open("profile.html") as f:
        content = f.read()
        return content


@app.route("/profile4/<aaa>")
def profile4(aaa):
    with open("profile.html") as f:
        content = f.read()
        return content


@app.route("/profile5/<user_name>")
def profile5(user_name):
    with open("profile.html") as f:
        content = f.read()
        return content


@app.route("/profile8/<user_name>")  # int:不是说user_id变量名是int类型，而是过滤器（后面详细讲）
def profile8(user_name):  # 如果想调用profile7成功，那么只有user_id这个位置的是是纯数字的时候 才可以调用
    # 1. 打开profile.html
    with open("profile.html") as f:
        # 2. 读取其内容
        content = f.read()

    # 将显示用户名字的位置替换为账号
    content = content.replace("liyichun", user_name)

    # 3. 将内容当做字符串 返回
    # return "这是个人中心页面2222"
    return content


@app.route("/profile9/<int:user_id>")
def profile9(user_id):
    # 如果一个函数它是一个普通的功能的话，它一般包括2个功能：
    # 1. 业务方面的数据处理（对变量user_id的使用，例如查询数据库等）
    # 2. 模板的替换(对应profile.html的获取以及字符的替换)

    # 1. 打开profile.html
    with open("profile.html") as f:
        # 2. 读取其内容
        content = f.read()

    # 将显示用户名字的位置替换为账号
    content = content.replace("liyichun", str(user_id))

    # 3. 将内容当做字符串 返回
    return content


@app.route("/profile10/<user_name>")
def profile10(user_name):
    # 使用模板（jinjia2模板引擎）,可以对html的数据处理进行了封装
    # 注意：它会默认到templates文件夹中取找 profile.html
    return render_template("profile.html", user_name=user_name)


app.run()